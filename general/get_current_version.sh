#!/bin/sh
set -e

BASEDIR=$(dirname $0)

VERSION_TYPE=$2
VERSION=$1

if [ -z $VERSION_TYPE ]; then
  VERSION_TYPE="prerelease"
fi

if [ -z $VERSION ]; then
  echo "[ERROR] no version has been passed as argument. Please use like this ./get_current_version.sh [VERSION] [VERSION_TYPE]"
fi

if [ $VERSION_TYPE = "major" ]; then
  echo $(echo $VERSION | awk -F "-" '{print $1}' | awk -F "." '{print $1}' )
elif [ $VERSION_TYPE = "minor" ]; then
  echo $(echo $VERSION | awk -F "-" '{print $1}' | awk -F "." '{print $1"."$2}' )
elif [ $VERSION_TYPE = "patch" ]; then
  echo $(echo $VERSION | awk -F "-" '{print $1}' | awk -F "." '{print $1"."$2"."$3}' )
else
  echo $VERSION
fi
