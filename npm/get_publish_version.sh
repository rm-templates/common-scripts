#!/bin/sh
set -e

BASEDIR=$(dirname $0)

# RELEASE TYPE 
#   can be production OR development
RELEASE_TYPE=$1

# MODE
#   return        will return the version the package will be published under
#   tag_only      will only return the tag that should be used for dev packages
#   log           will only log determined variables
MODE=$2

# DEV_TAG_PREFIX
#   prefix for development builds. 1.0.1 e.g. will be tagged as dev-1.0.1
#   with dev as DEV_TAG_PREFIX. Default is dev
DEV_TAG_PREFIX=$3


# MAIN_TAG
#   Sets the tag for production build. Default value is latest
MAIN_TAG=$4


MODE=${MODE:-"return"}

RELEASE_TYPE=${RELEASE_TYPE:-"development"}

DEV_TAG_PREFIX=${DEV_TAG_PREFIX:-dev}
MAIN_TAG=${MAIN_TAG:-latest}

npm config set git-tag-version=false

PACKAGE_NAME=$(npm run env | grep "npm_package_name" | awk -F "=" '{print $2}')

CURRENT_VERSION_LOCAL=$($BASEDIR/get_current_version.sh)
CURRENT_VERSION_PATCH_LOCAL=$($BASEDIR/get_current_version.sh patch $CURRENT_VERSION_LOCAL)

DEV_TAG="$DEV_TAG_PREFIX-$CURRENT_VERSION_PATCH_LOCAL"

CURRENT_VERSION_REMOTE_MAIN=$(npm view $PACKAGE_NAME version)
CURRENT_VERSION_REMOTE_DEV=$(npm view $PACKAGE_NAME version --tag $DEV_TAG || echo "")

CURRENT_VERSION_PATCH_REMOTE_MAIN=$($BASEDIR/get_current_version.sh patch $CURRENT_VERSION_REMOTE_MAIN)
CURRENT_VERSION_PATCH_REMOTE_DEV=$($BASEDIR/get_current_version.sh patch ${CURRENT_VERSION_REMOTE_DEV:-0.0.0})

PUBLISH_TAG=$([ "$RELEASE_TYPE" = "production" ] && echo "$MAIN_TAG" || echo "$DEV_TAG")

if [ "$MODE" = "log" ]; then
  echo """
  MODE:                               $MODE
  RELEASE_TYPE:                       $RELEASE_TYPE
  PACKAGE_NAME:                       $PACKAGE_NAME

  CURRENT_VERSION_LOCAL:              $CURRENT_VERSION_LOCAL
  CURRENT_VERSION_REMOTE_MAIN:        $CURRENT_VERSION_REMOTE_MAIN
  CURRENT_VERSION_REMOTE_DEV:         $CURRENT_VERSION_REMOTE_DEV

  CURRENT_VERSION_PATCH_LOCAL:        $CURRENT_VERSION_PATCH_LOCAL
  CURRENT_VERSION_PATCH_REMOTE_MAIN:  $CURRENT_VERSION_PATCH_REMOTE_MAIN
  CURRENT_VERSION_PATCH_REMOTE_DEV:   $CURRENT_VERSION_PATCH_REMOTE_DEV

  DEV_TAG:                            $DEV_TAG
  MAIN_TAG:                           $MAIN_TAG
  PUBLISH_TAG:                        $PUBLISH_TAG
  """
  exit 0
fi

if [ "$MODE" = "tag_only" ]; then
  echo "$PUBLISH_TAG"
  exit 0
fi


if [ "$RELEASE_TYPE" = "production" ]; then
  echo $CURRENT_VERSION_LOCAL  
else
  if [ "$CURRENT_VERSION_PATCH_LOCAL" != "$CURRENT_VERSION_PATCH_REMOTE_DEV" ]; then
    VERSION="$CURRENT_VERSION_PATCH_LOCAL-0"
    echo "$VERSION"

  else
    npm version $CURRENT_VERSION_REMOTE_DEV --allow-same-version > /dev/null
    npm version prerelease > /dev/null
    
    echo "$($BASEDIR/get_current_version.sh)"
  fi
fi