#!/bin/sh
set -e

BASEDIR=$(dirname $0)

VERSION_TYPE=$1
VERSION=$2

PACKAGE_JSON=${PACKAGE_JSON:-"$BASEDIR/../../package.json"}

if [ -z $VERSION_TYPE ]; then
  VERSION_TYPE="prerelease"
fi

if [ -z $VERSION ]; then
  VERSION=$(cat $PACKAGE_JSON | grep '"version"' | head -n 1 | awk '{print $2}' | sed 's/"//g; s/,//g')
fi

echo $(${BASEDIR}/../general/get_current_version.sh $VERSION $VERSION_TYPE)