#!/bin/bash

BASEDIR=$(dirname $0)
GITLAB_HOST=${GITLAB_HOST:-gitlab.com}

if [[ -n "$CI_JOB_TOKEN" ]]; then
  echo "detected CI_JOB_TOKEN. setting authorization for //${GITLAB_HOST}/api/v4/packages/npm/:_authToken"
  npm config -- set "//${GITLAB_HOST}/api/v4/packages/npm/:_authToken" "${CI_JOB_TOKEN}"
fi