
#!/bin/bash

BASEDIR=$(dirname $0)
GITLAB_HOST=${GITLAB_HOST:-gitlab.com}

if [[ -z $CI_PROJECT_ID ]]; then
  exit 0
fi

if [[ -z $CI_JOB_TOKEN ]]; then
  exit 0
fi

NPM_SCOPE=$(node $BASEDIR/node/get_scope.js ../../../package.json)

if [[ -z $NPM_SCOPE ]]; then
  exit 0
else
  echo "" >> .npmrc
  echo $NPM_SCOPE:registry=https://${GITLAB_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/ >> .npmrc
  echo //${GITLAB_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN} >> .npmrc
fi

