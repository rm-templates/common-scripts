if [[ -z $NPM_ACCESS_TOKEN ]]; then
  exit 0
else
  echo "NPM_ACCESS_TOKEN found. Setting npm registry credentials"

  npm config set //registry.npmjs.org/:_authToken $NPM_ACCESS_TOKEN
fi